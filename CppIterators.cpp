#include <iostream>
#include <list>
#include <map>
#include <set>
#include <vector>
using namespace std;

//Step4
void displayVectorValues4()
{
    std::vector<int> val{ 1, 2, 3, 4 };
    auto it1 = val.rbegin(); 
    while(it1 != val.rend()) {
        std::cout << *it1 << std::endl;
        it1++;
    }
}

// Step3.
void displayVectorValues3()
{

    vector<int> nums1{ 1, 2, 3, 4, 5 };

    vector<int>::iterator it = nums1.begin();
    cout << *it << endl;

    ++it;
    cout << *it << endl;
}

// Step3.
void displayVectorValues2()
{

    vector<int> nums1{ 1, 2, 3, 4, 5 };

    vector<int>::iterator it = nums1.begin();
    while(it != nums1.end()) {
        cout << *it << endl;
        it++;
    }
}

// Step2.
void displayVectorValues1()
{
    vector<int> nums1{ 1, 2, 3, 4, 5 };

    for(vector<int>::iterator i = nums1.begin(); i != nums1.end(); i++) {
        cout << *i << endl;
    }
}

// Step1.
int main()
{
    displayVectorValues3();
    return 0;
}