#include <iostream>
#include <string>
using namespace std;

class Employee{
public:
    std::string name;
    int id;

    void displayId(){
        cout<<id<<endl;
    }
    
	//Default constructor.
    /*Employee(){
        cout<<"I am constructor!!!"<<endl;
    }*/
    
    Employee(){
        cout<<"I am constructor!!!"<<endl;
        id = 100;
    }
}; 

 
int main(){
    Employee s1;
    s1.displayId();
}
