#include <iostream>
#include <string>
using namespace std;

int max(int val1, int val2){
    return (val1>val2)?val1:val2;
}

double max(double val1, double val2){
    return (val1>val2)?val1:val2;
}

int main(){
    cout<<max(10,20)<<endl;
    cout<<max(100.5,200.1)<<endl;
    return 0;
}

//================================================
//1.
template <typename X>
void swapping(X &A, X &B){
    X temp = A;
    A = B;
    B = temp;
}

int main(){
   
    int A = 10, B = 20;
    swapping<int>(A,B);
    
    char C1 = 'X', C2 = 'y';
    swapping<char>(C1,C2);
    
    double D1 = 10.5,D2 =11.1;
    swapping<double>(D1,D2);
    
    return 0;
}

//================================================

//2. Generic function with multiple parameters
template <class type1, class type2>

void display(type1 x, type2 y){
    cout<<x<<endl;
	cout<<y<<endl;
}

int main(){
   
    display(10,10);
	display(10,"Hello");
    
    return 0;
}
 
//================================================ 

template <class X, class Y>

class test{
  X a;
  Y b;
public:
    test(X x, Y y){
        a = x;
        b = y;
    }
    
    void display(){
        cout<<a<<endl;
        cout<<b<<endl;
    }
};


int main(){
   
    test<int, char> t1(10,'A');
    t1.display();
}
 
//================================================

#include <iostream>
#include <string>
using namespace std;

template <typename T>
T maxValue(T val1, T val2){
    return (val1>val2)?val1:val2;
}

struct Student {
    std::string name;
    int marks;
    bool operator>(const Student &rhs) const {
        return this->marks > rhs.marks;
    }
};

int main(){
    int a = 10;
    int b = 20;
    cout << maxValue<int>(a,b)<<endl;
    cout<<max<double>(100.5,200.1)<<endl;
    
    Student p1{"Java",10};
    Student p2{"CPP",50};
    Student p3 = maxValue<Student>(p1,p2);
    
    cout<<p3.name<<endl;
    
    return 0;
}

//================================================

#include <iostream>
#include <string>
using namespace std;

template <class X>
void swapping(X &A, X &B){
    X temp = A;
    A = B;
    B = temp;
    cout<< "Generic Swap \n";
}

void swapping(int &A, int &B){
    int temp = A;
    A = B;
    B = temp;
    
    cout<< "Int Specialized Swap \n";
}

int main(){
   
    int A = 10, B = 20;
    swapping<int>(A,B);
    
    char C1 = 'X', C2 = 'y';
    swapping<char>(C1,C2);
    
    double D1 = 10.5,D2 =11.1;
    swapping<double>(D1,D2);
    
    return 0;
}