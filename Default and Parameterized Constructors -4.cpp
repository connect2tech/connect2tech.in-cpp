#include <iostream>
#include <string>
using namespace std;

class Employee{
public:
    std::string name;
    int id;

	//Connstructor with default values
    Employee(std::string name1="None", int id1=0){
        name = name1;
        id = id1;
        cout << "constructor2" << endl;
    }
}; 

 
int main(){
    Employee s1;
    cout << s1.name << endl;
    cout << s1.id << endl;
    
    Employee s2("Arnab");
    cout << s2.name << endl;
    cout << s2.id << endl;
    
    Employee s3("James",100);
    cout << s3.name << endl;
    cout << s3.id << endl;
}
