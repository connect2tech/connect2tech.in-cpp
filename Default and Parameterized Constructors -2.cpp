#include <iostream>
#include <string>
using namespace std;

class Employee{
public:
    std::string name;
    int id;

    void displayId(){
        cout<<id<<endl;
    }
    
	//Parameterized constructor
    Employee(std::string name1, int id1){
        name = name1;
        id = id1;
        cout << "constructor" << endl;
    }
    
     Employee(int id1,std::string name1){
        name = name1;
        id = id1;
        cout << "constructor" << endl;
    }
    
}; 

 
int main(){
    Employee s1("James",25);
    s1.displayId();
    
    Employee s2(30, "Jacob");
    s2.displayId();
}
