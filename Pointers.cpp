#include <iostream>
#include <string>
using namespace std;

int main()
{
    int *p{nullptr};
    int val{10};
    
    p = &val;
    
    cout<<p<<endl;
    cout<<&val<<endl;
    cout<<*p<<endl;

    return 0;
}