#include <iostream> 
#include <vector>
using namespace std; 

/*
int main(){
   vector<int> vector_example;
   
   for(int i=10; i>=5;i--){
       vector_example.push_back(i);
   }
   
   cout<< "Understanding Iterators: " << endl;
   
   for(auto i = vector_example.begin(); i!=vector_example.end();i++){
       cout << *i << " ";
   }
   
   return 0;
}
*/
//----------------------------------

/*
int main(){
   vector<int> vector_example;
   
   for(int i=10; i>=5;i--){
       vector_example.push_back(i);
   }
   
   cout<< "Understanding Iterators: " << endl;
   
   for(auto i = vector_example.begin(); i!=vector_example.end();i++){
       cout << *i << " ";
   }
   
   cout << "\n";
   
   vector_example.pop_back(); 
   for(auto i = vector_example.begin(); i!=vector_example.end();i++){
       cout << *i << " ";
   }
   
   vector_example.push_back(100); 
   for(auto i = vector_example.begin(); i!=vector_example.end();i++){
       cout << *i << " ";
   }
   
   return 0;
}
*/