#include<iostream> 
using namespace std; 

class StaticClassFuncation 
{ 
public: 
	
     static void function2(){
        cout<<"static function2"<<endl;
    }
    
    static void function1(){
        cout<<"static function1"<<endl;
        function2();
    }
}; 

int main() 
{ 
    
    StaticClassFuncation::function1(); 
} 

/*
class StaticClassMember 
{ 
public: 
	static int i; 
}; 

int main() 
{ 
    StaticClassMember m1; 
    StaticClassMember m2; 
    m1.i =2; 
    m2.i = 3; 
        
    // prints value of i 
    std::cout<<m1.i;
    std::cout<<m2.i;
} 
*/



/*  
void staticVariableFunction() 
{  
    // static variable 
    static int count = 0; 
    
    cout<<count<< endl;
    
    count++; 
} 
  
int main() 
{ 
    staticVariableFunction(); 
    staticVariableFunction(); 
    return 0; 
} */

