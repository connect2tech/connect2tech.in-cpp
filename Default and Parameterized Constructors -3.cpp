#include <iostream>
#include <string>
using namespace std;

class Employee{
public:
    std::string name;
    int id;

	//Initializer list
    Employee(std::string name1, int id1):name{name1},id{id1}{
        cout << "constructor2" << endl;
    }
}; 

 
int main(){
    Employee s1;
    cout << s1.name << endl;
    cout << s1.id << endl;
    
    Employee s2("Arnab");
    cout << s2.name << endl;
    cout << s2.id << endl;
    
    Employee s3("James",100);
    cout << s3.name << endl;
    cout << s3.id << endl;
}
